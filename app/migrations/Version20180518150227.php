<?php declare(strict_types = 1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518150227 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE odiseo_vendor DROP FOREIGN KEY FK_B506F54F72F5A1AA');
        $this->addSql('ALTER TABLE odiseo_product DROP FOREIGN KEY FK_61B72B43F603EE73');
        $this->addSql('ALTER TABLE odiseo_vendor_translation DROP FOREIGN KEY FK_5F5AE1AB2C2AC5D3');
        $this->addSql('CREATE TABLE sylius_channel (id INT AUTO_INCREMENT NOT NULL, default_locale_id INT NOT NULL, base_currency_id INT NOT NULL, default_tax_zone_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, color VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, enabled TINYINT(1) NOT NULL, hostname VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, theme_name VARCHAR(255) DEFAULT NULL, tax_calculation_strategy VARCHAR(255) NOT NULL, contact_email VARCHAR(255) DEFAULT NULL, skipping_shipping_step_allowed TINYINT(1) NOT NULL, skipping_payment_step_allowed TINYINT(1) NOT NULL, account_verification_required TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_16C8119E77153098 (code), INDEX IDX_16C8119E743BF776 (default_locale_id), INDEX IDX_16C8119E3101778E (base_currency_id), INDEX IDX_16C8119EA978C17 (default_tax_zone_id), INDEX IDX_16C8119EE551C011 (hostname), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sylius_product (id INT AUTO_INCREMENT NOT NULL, main_taxon_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, enabled TINYINT(1) NOT NULL, variant_selection_method VARCHAR(255) NOT NULL, average_rating DOUBLE PRECISION DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_677B9B7477153098 (code), INDEX IDX_677B9B74731E505 (main_taxon_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_channel ADD CONSTRAINT FK_16C8119E743BF776 FOREIGN KEY (default_locale_id) REFERENCES sylius_locale (id)');
        $this->addSql('ALTER TABLE sylius_channel ADD CONSTRAINT FK_16C8119E3101778E FOREIGN KEY (base_currency_id) REFERENCES sylius_currency (id)');
        $this->addSql('ALTER TABLE sylius_channel ADD CONSTRAINT FK_16C8119EA978C17 FOREIGN KEY (default_tax_zone_id) REFERENCES sylius_zone (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE sylius_product ADD CONSTRAINT FK_677B9B74731E505 FOREIGN KEY (main_taxon_id) REFERENCES sylius_taxon (id)');
        $this->addSql('DROP TABLE odiseo_channel');
        $this->addSql('DROP TABLE odiseo_product');
        $this->addSql('DROP TABLE odiseo_vendor');
        $this->addSql('DROP TABLE odiseo_vendor_translation');
        $this->addSql('ALTER TABLE sylius_customer CHANGE customersolde customersolde INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_product_association ADD CONSTRAINT FK_48E9CDAB4584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_association_product ADD CONSTRAINT FK_A427B9834584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_attribute_value ADD CONSTRAINT FK_8A053E544584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_image ADD CONSTRAINT FK_88C64B2D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_promotion_channels ADD CONSTRAINT FK_1A044F6472F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_taxon ADD CONSTRAINT FK_169C6CD94584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_payment_method_channels ADD CONSTRAINT FK_543AC0CC72F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_review ADD CONSTRAINT FK_C7056A994584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_variant ADD CONSTRAINT FK_A29B5234584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_shipping_method_channels ADD CONSTRAINT FK_2D98333572F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_channel_currencies ADD CONSTRAINT FK_AE491F9372F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_channel_locales ADD CONSTRAINT FK_786B7A8472F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_order ADD CONSTRAINT FK_6196A1F972F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id)');
        $this->addSql('ALTER TABLE sylius_product_translation ADD CONSTRAINT FK_105A9082C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_channels ADD CONSTRAINT FK_F9EF269B4584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_channels ADD CONSTRAINT FK_F9EF269B72F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sylius_product_options ADD CONSTRAINT FK_2B5FF0094584665A FOREIGN KEY (product_id) REFERENCES sylius_product (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_promotion_channels DROP FOREIGN KEY FK_1A044F6472F5A1AA');
        $this->addSql('ALTER TABLE sylius_payment_method_channels DROP FOREIGN KEY FK_543AC0CC72F5A1AA');
        $this->addSql('ALTER TABLE sylius_shipping_method_channels DROP FOREIGN KEY FK_2D98333572F5A1AA');
        $this->addSql('ALTER TABLE sylius_channel_currencies DROP FOREIGN KEY FK_AE491F9372F5A1AA');
        $this->addSql('ALTER TABLE sylius_channel_locales DROP FOREIGN KEY FK_786B7A8472F5A1AA');
        $this->addSql('ALTER TABLE sylius_order DROP FOREIGN KEY FK_6196A1F972F5A1AA');
        $this->addSql('ALTER TABLE sylius_product_channels DROP FOREIGN KEY FK_F9EF269B72F5A1AA');
        $this->addSql('ALTER TABLE sylius_product_association DROP FOREIGN KEY FK_48E9CDAB4584665A');
        $this->addSql('ALTER TABLE sylius_product_association_product DROP FOREIGN KEY FK_A427B9834584665A');
        $this->addSql('ALTER TABLE sylius_product_attribute_value DROP FOREIGN KEY FK_8A053E544584665A');
        $this->addSql('ALTER TABLE sylius_product_image DROP FOREIGN KEY FK_88C64B2D7E3C61F9');
        $this->addSql('ALTER TABLE sylius_product_taxon DROP FOREIGN KEY FK_169C6CD94584665A');
        $this->addSql('ALTER TABLE sylius_product_review DROP FOREIGN KEY FK_C7056A994584665A');
        $this->addSql('ALTER TABLE sylius_product_variant DROP FOREIGN KEY FK_A29B5234584665A');
        $this->addSql('ALTER TABLE sylius_product_translation DROP FOREIGN KEY FK_105A9082C2AC5D3');
        $this->addSql('ALTER TABLE sylius_product_channels DROP FOREIGN KEY FK_F9EF269B4584665A');
        $this->addSql('ALTER TABLE sylius_product_options DROP FOREIGN KEY FK_2B5FF0094584665A');
        $this->addSql('CREATE TABLE odiseo_channel (id INT AUTO_INCREMENT NOT NULL, default_locale_id INT NOT NULL, base_currency_id INT NOT NULL, default_tax_zone_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, color VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, enabled TINYINT(1) NOT NULL, hostname VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, theme_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, tax_calculation_strategy VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, contact_email VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, skipping_shipping_step_allowed TINYINT(1) NOT NULL, skipping_payment_step_allowed TINYINT(1) NOT NULL, account_verification_required TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_1004A1A977153098 (code), INDEX IDX_1004A1A9743BF776 (default_locale_id), INDEX IDX_1004A1A93101778E (base_currency_id), INDEX IDX_1004A1A9A978C17 (default_tax_zone_id), INDEX IDX_1004A1A9E551C011 (hostname), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE odiseo_product (id INT AUTO_INCREMENT NOT NULL, main_taxon_id INT DEFAULT NULL, vendor_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, enabled TINYINT(1) NOT NULL, variant_selection_method VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, average_rating DOUBLE PRECISION DEFAULT \'0\' NOT NULL, UNIQUE INDEX UNIQ_61B72B4377153098 (code), INDEX IDX_61B72B43731E505 (main_taxon_id), INDEX IDX_61B72B43F603EE73 (vendor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE odiseo_vendor (id INT AUTO_INCREMENT NOT NULL, channel_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, slug VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, email VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, logo_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, enabled TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_B506F54F989D9B62 (slug), INDEX IDX_B506F54F72F5A1AA (channel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE odiseo_vendor_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, description LONGTEXT NOT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX odiseo_vendor_translation_uniq_trans (translatable_id, locale), INDEX IDX_5F5AE1AB2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE odiseo_channel ADD CONSTRAINT FK_1004A1A93101778E FOREIGN KEY (base_currency_id) REFERENCES sylius_currency (id)');
        $this->addSql('ALTER TABLE odiseo_channel ADD CONSTRAINT FK_1004A1A9743BF776 FOREIGN KEY (default_locale_id) REFERENCES sylius_locale (id)');
        $this->addSql('ALTER TABLE odiseo_channel ADD CONSTRAINT FK_1004A1A9A978C17 FOREIGN KEY (default_tax_zone_id) REFERENCES sylius_zone (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE odiseo_product ADD CONSTRAINT FK_61B72B43731E505 FOREIGN KEY (main_taxon_id) REFERENCES sylius_taxon (id)');
        $this->addSql('ALTER TABLE odiseo_product ADD CONSTRAINT FK_61B72B43F603EE73 FOREIGN KEY (vendor_id) REFERENCES odiseo_vendor (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE odiseo_vendor ADD CONSTRAINT FK_B506F54F72F5A1AA FOREIGN KEY (channel_id) REFERENCES odiseo_channel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE odiseo_vendor_translation ADD CONSTRAINT FK_5F5AE1AB2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES odiseo_vendor (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE sylius_channel');
        $this->addSql('DROP TABLE sylius_product');
        $this->addSql('ALTER TABLE sylius_customer CHANGE customersolde customersolde VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
